const User = require('../models/users');
const jwt = require('jsonwebtoken');
const config = require('../config');

function createJWT(user) {
    return token = jwt.sign({ user: user._id }, config.jwtSecret);
}

async function register(req, res) {
    try {
        if (!req.body.email) {
            throw { number: 400, message: 'missing email' };
        }
        if (!req.body.password) {
            throw { number: 400, message: 'missing password' };
        }
        const user = new User(req.body);
        await user.save();
        res.status(200).json({ user, token: createJWT(user) });
    } catch (e) {
        console.log(e);
        const message = e.message || 'error creating user';
        res.status(e.number || 500).json({ error: message });
    }
}

async function login(req, res) {
    try {
        if (!req.body) {
            throw { number: 400, message: 'missing body' };
        }
        user = await User.findOne({ email: req.body.email })
        if (user) {
            const isVerify = user.verifyPasswordSync(req.body.password)
            if (!isVerify) {
                throw { number: 401, message: 'cannot verify user' }
            }
        } else {
            throw { number: 404, message: 'user not found' }
        }
        res.status(200).json({ user, token: createJWT(user) })
    } catch (e) {
        console.log(e)
        const message = e.message || 'error getting user';
        res.status(e.number || 500).json({ error: message });
    }
}

async function updateUser(_id, update) {
    try {
        return User.updateOne({ _id }, update);
    } catch (e) {
        throw e;
    }
}

async function findUser(query) {
    try {
        return User.findOne(query);
    } catch (e) {
        throw e;
    }
}

async function removeTeamRef(id) {
    try {
        return User.updateMany({}, { $pull: { teams: id } }, { multi: true });
    } catch (e) {
        throw e;
    }
}

module.exports = {
    register,
    login,
    updateUser,
    findUser,
    removeTeamRef,
}
