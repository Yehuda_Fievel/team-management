const Team = require('../models/teams');
const userCtrl = require('../contollers/users');


async function create(req, res) {
    try {
        if (!req.body.manager) {
            return res.status(400).json({ message: 'must include manager' });
        }
        const team = new Team(req.body);
        team.managers = [req.body.manager];
        const userPromise = userCtrl.updateUser(req.user, { $addToSet: { teams: team } });
        await Promise.all([team.save(), userPromise]);
        res.status(200).json(team);
    } catch (e) {
        console.log(e);
        res.status(500).json({ error: 'Error creating team' });
    }
}

async function isManager(req, res, next) {
    try {
        const team = await Team.findOne({ _id: req.params.id });
        if (!team) {
            return res.status(404).json({ message: 'no team found' });
        }
        
        if (!team.managers.includes(req.user._id)) {
            return res.status(403).json({ message: 'not a manager on this team' });
        }
        req.team = team;
        next();
    } catch (e) {
        console.log(e);
        res.status(500).json({ error: 'Error updating team' });
    }
}

async function addPlayer(req, res) {
    try {
        const { team, body } = req
        const player = body.player;
        if (team.players.includes(player)) {
            return res.status(400).json({ message: 'the player is already on this team' });
        }
        team.players.push(player);
        await team.save();
        res.status(200).js1zon(team);
    } catch (e) {
        console.log(e);
        res.status(500).json({ error: 'Error updating team' });
    }
}

async function removePlayer(req, res) {
    try {
        const team = req.team;
        team.players = req.team.players.find(player => player != req.body.player);
        await team.save();
        res.status(200).json(team);
    } catch (e) {
        console.log(e);
        res.status(500).json({ error: 'Error updating team' });
    }
}

async function addManager(req, res) {
    try {
        const { team, body } = req;
        const manager = body.manager;
        if (team.managers.includes(manager)) {
            return res.status(400).json({ message: 'the player is already on a manager' });
        }
        team.managers.push(manager);
        await team.save();
        res.status(200).json(team);
    } catch (e) {
        console.log(e);
        res.status(500).json({ error: 'Error updating team' });
    }
}

async function removeManager(req, res) {
    try {
        if (req.user.teams.includes(req.params.id)) {
            return res.status(400).json({ message: 'you cannot remove yourself from your own team' });
        }
        const team = req.team;
        team.managers = req.team.managers.find(player => player != req.body.manager);
        await team.save();
        res.status(200).json(team);
    } catch (e) {
        console.log(e);
        res.status(500).json({ error: 'Error updating team' });
    }
}

async function del(req, res) {
    try {
        if (req.user.teams.includes(req.user._id)) {
            return res.status(403).json({ message: 'you can only delete your own team' });
        }
        const team = await Team.findOneAndDelete({ _id: req.params.id });
        await userCtrl.removeTeamRef(team._id);
        res.sendStatus(200);
    } catch (e) {
        console.log(e);
        res.status(500).json({ error: 'Error updating team' });
    }
}

module.exports = {
    create,
    isManager,
    addPlayer,
    removePlayer,
    addManager,
    removeManager,
    del,
}
