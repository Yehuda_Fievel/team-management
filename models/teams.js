// import npm modules
const mongoose = require('mongoose');
const bcrypt = require('mongoose-bcrypt');

const Schema = mongoose.Schema;


const schema = new Schema({
    name: {
        type: String,
        required: true,
        index: true,
        unique: true,
    },
    players: [{
        type: Schema.ObjectId,
        ref: 'User'
    }],
    managers: [{
        type: Schema.ObjectId,
        ref: 'User'
    }],
}, { timestamps: true });


schema.plugin(bcrypt); // adds bcrypt

module.exports = exports = mongoose.model('Teams', schema, 'teams'); // export model for use
