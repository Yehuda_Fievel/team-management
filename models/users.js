// import npm modules
const mongoose = require('mongoose');
const bcrypt = require('mongoose-bcrypt');

const Schema = mongoose.Schema;


const schema = new Schema({
    email: {
        type: String,
        required: true,
        index: true,
        unique: true,
    },
    teams: [{
        type: Schema.ObjectId,
        ref: 'Team'
    }],
}, { timestamps: true })


schema.plugin(bcrypt); // adds bcrypt

module.exports = exports = mongoose.model('User', schema, 'users'); // export model for use
