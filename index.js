const express = require('express');;
const app = express();
const mongoose = require('mongoose');
const config = require('./config');

app.use(express.json());

// Enabling middleware
require('./passport')(app, {});

// Enabling routes
require('./routes')(app, {});


// connect to mongo
mongoose.connect(config.mongodb, { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true });

// make mongo use the global promise 
mongoose.Promise = global.Promise;

mongoose.connection.on('connected', function () {
    console.log('Mongo connection established successfully');
})

// bind mongoose to error event (to get notification of connection errors)
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.listen(config.port, () => console.log(`Example app listening on port ${config.port}!`));
