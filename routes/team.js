const express = require('express');
const router = express.Router();
const teams = require('../contollers/teams');

router.post('/', teams.create);
router.put('/:id/players/add', teams.isManager, teams.addPlayer);
router.put('/:id/players/remove', teams.isManager, teams.removePlayer);
router.put('/:id/managers/add', teams.isManager, teams.addManager);
router.put('/:id/managers/remove', teams.isManager, teams.removeManager);
router.delete('/:id', teams.isManager, teams.del);


module.exports = router;
