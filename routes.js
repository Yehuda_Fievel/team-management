const passport = require("passport");


module.exports = function (app) {
    app.use('/team', ensureAuthenticated, require('./routes/team'));
    app.use('/user', require('./routes/user'));

    function ensureAuthenticated(req, res, next) {
        passport.authenticate('jwt', { session: false })(req, res, next);
    }
}
